class Calculator {
  // ...
  // define add() function
  // def add(arg1, arg2)
  // end
}
const calculator = new Calculator();

describe("calculator", () => {
  it("adds correctly", () => {
    expect(calculator.add(1,1)).toEqual(2)
  })
})
